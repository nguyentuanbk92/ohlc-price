# Historical OHLC Price Data

## 1. Requirements
- We have just purchased a large amount of historical OHLC price data that were shared to us in CSV files format.
- We need to start centralising and digitalising those data. These files can be ranging from a few GBs to a couple of TBs.
- Create a RESTful HTTP APIs
  - POST `/data`
  - GET `/data`

## 2. Architecture
The following diagram is introduced in Uncle Bob’s original blog about Clean Architecture

![clean-architecture](./public/images/clean-architecture.png)

We apply some principles of this architecture:
- Your component can only depend on one inner layer from the outer layer (layer design pattern)
- Each component should be coupled through the interface if the coupling is across the layers. And dependency must be solved by Dependency Injection

So we using the simple architecture:
![simple-clean-architecture](./public/images/simple-layers-clean-architecture-1.png)
- **Transport**: Handle HTTP requests, return data to Client
- **Business**: Implement business logic
- **Repository** (optional): Prepare needed data for business logic
- **Storage**: Store and retrieve data

## 3. How to start

### How to get default config file
```bash
go build -o ohlc-price ./cmd/ohlc-price

./ohlc-price -example
```

### How to run with custom config file
```bash
./ohlc-price -config-file <path/to/config-file>
```

### Using docker compose

Run docker compose with default config or you can change config if you want
```bash
docker compose up -d
```

## 4. Checklist
- [x] API `POST /data`
  - [x] Upload and Process the CSV file
  - [x] Only be able to receive CSV files with correct format
- [x] API `GET /data`
  - [x] Query the data from database
  - [x] Pagination and search using query string for data must be available (filter by symbol, unix timestamp)
- [x] Unit test
- [x] API `GET /import-process` (use to get all import process)
