package importprocessstorage

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/pkg/common"
)

func (s *sqlStore) List(ctx context.Context, args *importprocessmodel.ListImportProcessesArgs) ([]*importprocessmodel.ImportProcess, error) {
	var result []*importprocessmodel.ImportProcess
	db := s.db
	db = db.Table(importprocessmodel.ImportProcess{}.TableName())
	paging := args.Paging
	db = db.Offset((paging.Page - 1) * paging.Limit).Limit(paging.Limit)

	if err := db.Order("id DESC").Find(&result).Error; err != nil {
		return nil, common.ErrDB(err)
	}
	return result, nil
}
