package importprocessstorage

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/pkg/common"

	"gorm.io/gorm"
)

func (s *sqlStore) GetByCondition(ctx context.Context, conditions map[string]interface{}) (*importprocessmodel.ImportProcess, error) {
	var result importprocessmodel.ImportProcess

	db := s.db

	if err := db.Where(conditions).
		First(&result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &result, nil
}
