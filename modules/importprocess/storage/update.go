package importprocessstorage

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/pkg/common"
)

func (s *sqlStore) UpdateByUUID(ctx context.Context, uuid string, args *importprocessmodel.ImportProcess) error {
	db := s.db

	if err := db.Where("uuid = ?", uuid).Updates(args).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
