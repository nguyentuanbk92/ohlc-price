package importprocessstorage

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/pkg/common"
)

func (s *sqlStore) Create(ctx context.Context, impProcess *importprocessmodel.ImportProcess) error {
	db := s.db
	if err := db.Create(impProcess).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
