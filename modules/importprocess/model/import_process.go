package importprocessmodel

import (
	"ohlc-price/pkg/common"
	"time"
)

const EntityName = "import_process"

type ImportProcess struct {
	common.SQLModel
	CompletedAt    *time.Time `json:"completed_at" gorm:"column:completed_at"`
	State          string     `json:"state" gorm:"state"`
	OriginFileName string     `json:"origin_file_name" gorm:"origin_file_name"`
	ImportType     string     `json:"import_type" gorm:"import_type"`
	SuccessCount   int        `json:"success_count" gorm:"success_count"`
	ErrorCount     int        `json:"error_count" gorm:"error_count"`
	UUID           string     `json:"uuid" gorm:"column:uuid;index:idx_import_process_uuid`
}

func (ImportProcess) TableName() string {
	return "import_processes"
}
