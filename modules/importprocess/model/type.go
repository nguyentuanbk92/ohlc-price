package importprocessmodel

import (
	"ohlc-price/pkg/common"
	"time"
)

type ImportState string

const (
	STATE_CREATED    ImportState = "created"
	STATE_PROCESSING ImportState = "processing"
	STATE_COMPLETED  ImportState = "completed"
	STATE_FAILED     ImportState = "failed"
)

type ImportType string

const (
	IMPORT_PRICE ImportType = "price"
)

type CreateImportProcessArgs struct {
	CompletedAt    *time.Time  `json:"completed_at"`
	State          ImportState `json:"state"`
	OriginFileName string      `json:"origin_file_name"`
	ImportType     ImportType  `json:"import_type"`
	SuccessCount   int         `json:"success_count"`
	ErrorCount     int         `json:"error_count"`
	UUID           string      `json:"uuid"`
}

func (args *CreateImportProcessArgs) Validate() error {
	if args.ImportType == "" {
		return common.NewCustomError(
			nil,
			"Missing import type",
			"")
	}
	if args.OriginFileName == "" {
		return common.NewCustomError(
			nil,
			"Missing original file name",
			"")
	}
	if args.State == "" {
		args.State = STATE_CREATED
	}
	return nil
}

type ListImportProcessesArgs struct {
	Paging common.Paging
}

type UpdateImportProcessInfoArgs struct {
	CompletedAt  *time.Time  `json:"completed_at"`
	State        ImportState `json:"state"`
	ImportType   ImportType  `json:"import_type"`
	SuccessCount int         `json:"success_count"`
	ErrorCount   int         `json:"error_count"`
}

type ImportProcessingEventData struct {
	UUID           string
	OriginFileName string
	State          ImportState
	ImportType     ImportType
}

type ImportProcessCompletedEventData struct {
	UUID         string
	State        ImportState
	SuccessCount int        `json:"success_count"`
	ErrorCount   int        `json:"error_count"`
	CompletedAt  *time.Time `json:"completed_at"`
}
