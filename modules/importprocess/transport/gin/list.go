package ginimportprocess

import (
	"net/http"
	importprocessbiz "ohlc-price/modules/importprocess/biz"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	importprocessstorage "ohlc-price/modules/importprocess/storage"
	"ohlc-price/pkg/common"
	"ohlc-price/pkg/component"

	"github.com/gin-gonic/gin"
)

func ListImportProcess(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var args importprocessmodel.ListImportProcessesArgs

		if err := c.ShouldBind(&args); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		args.Paging.Fulfill()

		store := importprocessstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := importprocessbiz.NewListImProcessBiz(store)
		result, err := biz.List(c.Request.Context(), &args)
		if err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, result)
	}
}
