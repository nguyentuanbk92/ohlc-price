package importprocessbiz

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
)

type CreateImProcessStore interface {
	Create(ctx context.Context, args *importprocessmodel.ImportProcess) error
}

type createImProcessBiz struct {
	store CreateImProcessStore
}

func NewCreateImProcessBiz(store CreateImProcessStore) *createImProcessBiz {
	return &createImProcessBiz{store: store}
}

func (biz *createImProcessBiz) Create(ctx context.Context, args *importprocessmodel.CreateImportProcessArgs) error {
	if err := args.Validate(); err != nil {
		return err
	}

	imProcess := &importprocessmodel.ImportProcess{
		CompletedAt:    args.CompletedAt,
		State:          string(args.State),
		OriginFileName: args.OriginFileName,
		ImportType:     string(args.ImportType),
		SuccessCount:   args.SuccessCount,
		ErrorCount:     args.ErrorCount,
		UUID:           args.UUID,
	}
	return biz.store.Create(ctx, imProcess)
}
