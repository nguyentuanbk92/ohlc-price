package importprocessbiz

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/pkg/common"
)

type ListImProcessStore interface {
	List(ctx context.Context, args *importprocessmodel.ListImportProcessesArgs) ([]*importprocessmodel.ImportProcess, error)
}

type listImProcessBiz struct {
	store ListImProcessStore
}

func NewListImProcessBiz(store ListImProcessStore) *listImProcessBiz {
	return &listImProcessBiz{store: store}
}

func (biz *listImProcessBiz) List(ctx context.Context, args *importprocessmodel.ListImportProcessesArgs) ([]*importprocessmodel.ImportProcess, error) {
	result, err := biz.store.List(ctx, args)

	if err != nil {
		return nil, common.ErrCannotListEntity(importprocessmodel.EntityName, err)
	}

	return result, nil
}
