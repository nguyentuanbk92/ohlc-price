package importprocessbiz

import (
	"context"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/pkg/common"
)

type UpdateImProcessStore interface {
	UpdateByUUID(ctx context.Context, uuid string, args *importprocessmodel.ImportProcess) error
}

type updateImProcessBiz struct {
	store UpdateImProcessStore
}

func NewUpdateImProcessBiz(store UpdateImProcessStore) *updateImProcessBiz {
	return &updateImProcessBiz{store: store}
}

func (biz *updateImProcessBiz) UpdateInfoByUUID(ctx context.Context, uuid string, args *importprocessmodel.UpdateImportProcessInfoArgs) error {
	if uuid == "" {
		return common.NewCustomError(nil, "Misssing uuid", "")
	}
	cmd := &importprocessmodel.ImportProcess{
		CompletedAt:  args.CompletedAt,
		State:        string(args.State),
		SuccessCount: args.SuccessCount,
		ErrorCount:   args.ErrorCount,
	}
	return biz.store.UpdateByUUID(ctx, uuid, cmd)
}
