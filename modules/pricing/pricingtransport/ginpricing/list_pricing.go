package ginpricing

import (
	"net/http"
	"ohlc-price/modules/pricing/pricingbiz"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/modules/pricing/pricingstorage"
	"ohlc-price/pkg/common"
	"ohlc-price/pkg/component"

	"github.com/gin-gonic/gin"
)

func ListPricing(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var args pricingmodel.ListPricingsArgs

		if err := c.ShouldBind(&args); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		args.Paging.Fulfill()
		if err := args.Validate(); err != nil {
			panic(err)
		}

		store := pricingstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := pricingbiz.NewListPricingBiz(store)
		result, err := biz.List(c.Request.Context(), &args)
		if err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, result)
	}
}
