package ginpricing

import (
	"net/http"
	"ohlc-price/modules/pricing/pricingbiz/pricingimport"
	"ohlc-price/modules/pricing/pricingstorage"
	"ohlc-price/pkg/common"
	"ohlc-price/pkg/component"

	"github.com/gin-gonic/gin"
)

func HandleImportOHCLPrice(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		fileHeader, err := c.FormFile("file")
		// The file cannot be received.
		if err != nil {
			panic(common.NewCustomError(nil, "No file is received", "ErrNoFileReceived"))
			return
		}

		store := pricingstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := pricingimport.NewImportPricingBiz(appCtx.GetPubSub(), store)
		err = biz.Import(c.Request.Context(), fileHeader)
		if err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, gin.H{
			"message": "Import was processing.",
		})
	}
}
