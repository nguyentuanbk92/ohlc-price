package ginpricing

import (
	"net/http"
	"ohlc-price/modules/pricing/pricingbiz"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/modules/pricing/pricingstorage"
	"ohlc-price/pkg/common"
	"ohlc-price/pkg/component"

	"github.com/gin-gonic/gin"
)

func CreatePricing(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var args pricingmodel.CreatePricingArgs

		if err := c.ShouldBind(&args); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		store := pricingstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := pricingbiz.NewCreatePricingBiz(store)
		result, err := biz.Create(c.Request.Context(), &args)
		if err != nil {
			panic(err)
		}
		c.JSON(http.StatusCreated, result)
	}
}
