package pricingmodel

import (
	"fmt"
	"ohlc-price/pkg/common"
	"ohlc-price/pkg/enums/symbol_type"
)

type CreatePricingArgs struct {
	UnixTimestamp int64                  `json:"unix_timestamp"`
	High          float64                `json:"high"`
	Low           float64                `json:"low"`
	Open          float64                `json:"open"`
	Close         float64                `json:"close"`
	Symbol        symbol_type.SymbolType `json:"symbol"`
}

var (
	ErrHighPriceNotValid          = common.NewCustomError(nil, "High price is invalid", "ErrHighPriceNotValid")
	ErrLowPriceNotValid           = common.NewCustomError(nil, "Low price is invalid", "ErrLowPriceNotValid")
	ErrOpenPriceNotValid          = common.NewCustomError(nil, "Open price is invalid", "ErrOpenPriceNotValid")
	ErrClosePriceNotValid         = common.NewCustomError(nil, "Close price is invalid", "ErrClosePriceNotValid")
	ErrHighPriceLowerThanLowPrice = common.NewCustomError(nil, "High price can not lower than low price", "ErrHighPriceLowerThanLowPrice")
	ErrUnixTimeStampNotValid      = common.NewCustomError(
		nil,
		fmt.Sprintf("Unix timestamp does not valid"),
		"ErrUnixTimeStampNotValid",
	)
	ErrMissingSymbol = common.NewCustomError(
		nil,
		fmt.Sprintf("Missing symbol"),
		"ErrMissingSymbol",
	)

	ErrNotProvideBothFromAndTo = common.NewCustomError(
		nil,
		fmt.Sprintf("Please provide both from and to"),
		"ErrNotProvideBothFromAndTo",
	)
	ErrFromLowerThanTo = common.NewCustomError(
		nil,
		fmt.Sprintf("From must be greater than to"),
		"ErrFromLowerThanTo",
	)
)

func (args *CreatePricingArgs) Validate() error {
	if args.High <= 0 {
		return ErrHighPriceNotValid
	}
	if args.Low <= 0 {
		return ErrLowPriceNotValid
	}
	if args.Open <= 0 {
		return ErrOpenPriceNotValid
	}
	if args.Close <= 0 {
		return ErrClosePriceNotValid
	}
	if args.High < args.Low {
		return ErrHighPriceLowerThanLowPrice
	}
	if args.Symbol == "" {
		return ErrMissingSymbol
	}
	if !args.Symbol.IsValid() {
		return common.NewCustomError(
			nil,
			fmt.Sprintf("Symbol %s does not support", args.Symbol),
			"ErrSymbolNotSupport",
		)
	}
	if args.UnixTimestamp == 0 {
		return ErrUnixTimeStampNotValid
	}
	return nil
}

type ListPricingsArgs struct {
	Paging common.Paging
	From   *int64                 `json:"from" form:"from"`
	To     *int64                 `json:"to" form:"to"`
	Symbol symbol_type.SymbolType `json:"symbol" form:"symbol"`
}

func (args *ListPricingsArgs) Validate() error {
	if args.Symbol != "" && !args.Symbol.IsValid() {
		return common.NewCustomError(
			nil,
			fmt.Sprintf("Symbol %s does not support", args.Symbol),
			"ErrSymbolNotSupport",
		)
	}
	if (args.From != nil) != (args.To != nil) {
		return ErrNotProvideBothFromAndTo
	}
	if args.From != nil {
		if *args.From > *args.To {
			return ErrFromLowerThanTo
		}
	}
	return nil
}
