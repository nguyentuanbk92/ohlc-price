package pricingmodel

import (
	"ohlc-price/pkg/common"
)

const EntityName = "price"

type Price struct {
	common.SQLModel
	Symbol        string  `json:"symbol" gorm:"column:symbol;type:string;uniqueIndex:idx_pricing_unix_timestamp"`
	Open          float64 `json:"open" gorm:"column:open;"`
	High          float64 `json:"high" gorm:"column:high;"`
	Low           float64 `json:"low" gorm:"column:low;"`
	Close         float64 `json:"close" gorm:"column:close;"`
	UnixTimestamp int64   `json:"unix_timestamp" gorm:"column:unix_timestamp;uniqueIndex:idx_pricing_unix_timestamp"`
}

func (Price) TableName() string {
	return "prices"
}
