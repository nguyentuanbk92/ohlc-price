package pricingstorage

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/common"
)

func (s *sqlStore) List(ctx context.Context, args *pricingmodel.ListPricingsArgs) ([]*pricingmodel.Price, error) {
	var result []*pricingmodel.Price
	db := s.db
	db = db.Table(pricingmodel.Price{}.TableName())
	paging := args.Paging
	db = db.Offset((paging.Page - 1) * paging.Limit).Limit(paging.Limit)

	if args.Symbol != "" {
		db = db.Where("symbol = ?", args.Symbol)
	}
	if args.From != nil {
		db = db.Where("unix_timestamp >= ? AND unix_timestamp < ?", *args.From, *args.To)
	}

	if err := db.Order("id DESC").Find(&result).Error; err != nil {
		return nil, common.ErrDB(err)
	}
	return result, nil
}
