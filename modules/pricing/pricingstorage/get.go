package pricingstorage

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/common"

	"gorm.io/gorm"
)

func (s *sqlStore) GetByCondition(ctx context.Context, conditions map[string]interface{}) (*pricingmodel.Price, error) {
	var result pricingmodel.Price

	db := s.db

	if err := db.Where(conditions).
		First(&result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &result, nil
}
