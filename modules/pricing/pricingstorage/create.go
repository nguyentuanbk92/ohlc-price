package pricingstorage

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/common"
)

func (s *sqlStore) Create(ctx context.Context, price *pricingmodel.Price) (*pricingmodel.Price, error) {
	db := s.db
	if err := db.Create(price).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	return price, nil
}
