package pricingbiz

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/common"
)

type ListPricingStore interface {
	List(ctx context.Context, args *pricingmodel.ListPricingsArgs) ([]*pricingmodel.Price, error)
}

type listPricingBiz struct {
	store ListPricingStore
}

func NewListPricingBiz(store ListPricingStore) *listPricingBiz {
	return &listPricingBiz{store: store}
}

func (biz *listPricingBiz) List(ctx context.Context, args *pricingmodel.ListPricingsArgs) ([]*pricingmodel.Price, error) {
	result, err := biz.store.List(ctx, args)

	if err != nil {
		return nil, common.ErrCannotListEntity(pricingmodel.EntityName, err)
	}

	return result, nil
}
