package pricingimport

import (
	"ohlc-price/pkg/common/imcsv"
)

var columns = imcsv.Schema{
	{
		Name:     "UNIX",
		Code:     "unix",
		Optional: false,
	},
	{
		Name:     "SYMBOL",
		Code:     "symbol",
		Optional: false,
	},
	{
		Name:     "OPEN",
		Code:     "open",
		Optional: false,
	},
	{
		Name:     "HIGH",
		Code:     "high",
		Optional: false,
	},
	{
		Name:     "LOW",
		Code:     "low",
		Optional: false,
	},
	{
		Name:     "CLOSE",
		Code:     "close",
		Optional: false,
	},
}

type indexes struct {
	unix   int
	symbol int
	open   int
	high   int
	low    int
	close  int
}

func initIndexes(headerMap map[string]*imcsv.ColumnDef) indexes {
	return indexes{
		unix:   *headerMap["unix"].Index,
		symbol: *headerMap["symbol"].Index,
		open:   *headerMap["open"].Index,
		high:   *headerMap["high"].Index,
		low:    *headerMap["low"].Index,
		close:  *headerMap["close"].Index,
	}
}
