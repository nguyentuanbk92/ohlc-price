package pricingimport

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"mime/multipart"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/common"
	"ohlc-price/pkg/pubsub"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
)

type CreatePricingStore interface {
	Create(ctx context.Context, args *pricingmodel.Price) (*pricingmodel.Price, error)
}

type importPricingBiz struct {
	psub  pubsub.PubSub
	store CreatePricingStore
}

func NewImportPricingBiz(ps pubsub.PubSub, store CreatePricingStore) *importPricingBiz {
	return &importPricingBiz{psub: ps, store: store}
}

func (im *importPricingBiz) Import(ctx context.Context, fileHeader *multipart.FileHeader) error {
	// Retrieve file information
	extension := filepath.Ext(fileHeader.Filename)
	if !strings.Contains(extension, "csv") {
		panic(common.NewCustomError(nil, "Only support CSV file", ""))
	}

	file, err := fileHeader.Open()
	if err != nil {
		panic(common.NewCustomError(nil, "Can not read file", ""))
	}

	defer file.Close()

	concurrentWorker := 1000
	ch := make(chan []string, concurrentWorker)
	res := make(chan error)
	var wg sync.WaitGroup

	headerErrChan := make(chan error, 1)
	idxesChan := make(chan indexes, 1)

	go readFile(file, ch, headerErrChan, idxesChan)

	if headerErr := <-headerErrChan; headerErr != nil {
		return common.NewCustomError(
			headerErr,
			"File does not valid. Can not detect header",
			"",
		)
	}

	importUUID := uuid.New().String()
	{
		// public event to create an import process
		eventProcessing := &importprocessmodel.ImportProcessingEventData{
			UUID:           importUUID,
			OriginFileName: fileHeader.Filename,
			State:          importprocessmodel.STATE_PROCESSING,
			ImportType:     importprocessmodel.IMPORT_PRICE,
		}
		im.psub.Publish(ctx, common.TopicImportProcessing, pubsub.NewMessage(eventProcessing))
	}

	idxes := <-idxesChan

	// Inititial worker
	for i := 1; i <= concurrentWorker; i++ {
		wg.Add(1)
		go func(k int) {
			defer func() {
				wg.Done()
			}()
			im.worker(ctx, ch, idxes, res)
		}(i)
	}

	go func() {
		wg.Wait()
		close(res)
	}()

	go func() {
		errCount := 0
		successCount := 0
		for _err := range res {
			if _err != nil {
				errCount++
			} else {
				successCount++
			}
		}

		// public event to create an import process
		completedAt := time.Now()
		eventCompleted := &importprocessmodel.ImportProcessCompletedEventData{
			UUID:         importUUID,
			State:        importprocessmodel.STATE_COMPLETED,
			SuccessCount: successCount,
			ErrorCount:   errCount,
			CompletedAt:  &completedAt,
		}
		im.psub.Publish(ctx, common.TopicImportProcessCompleted, pubsub.NewMessage(eventCompleted))
	}()

	return nil
}

func (im *importPricingBiz) worker(ctx context.Context, ch <-chan []string,
	idxes indexes, res chan<- error) {
	for {
		select {
		case data, ok := <-ch:
			if !ok {
				return
			}
			price, err := parseStruct(idxes, data)
			if err == nil {
				_, err = im.store.Create(ctx, price)
			}
			res <- err
		}
	}
}

func readFile(f multipart.File, ch chan<- []string,
	headerErrChan chan<- error, idxesChan chan<- indexes) {
	// f, _ = os.Open("./public/templates/1000k.csv")
	fcsv := csv.NewReader(f)
	checkHeader := false
	defer close(ch)
	for {
		rStr, err := fcsv.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println("ERROR: ", err.Error())
			break
		}
		if !checkHeader {
			headerMap, err := columns.ValidateHeader(rStr)
			if err != nil {
				headerErrChan <- err
				return
			}
			headerErrChan <- nil
			idxesChan <- initIndexes(headerMap)
			checkHeader = true
			continue
		}
		ch <- rStr
	}
}

func parseStruct(idxes indexes, row []string) (*pricingmodel.Price, error) {
	open, err := strconv.ParseFloat(row[idxes.open], 64)
	if err != nil {
		return nil, common.NewCustomError(nil, "Open price not valid", "")
	}
	high, err := strconv.ParseFloat(row[idxes.high], 64)
	if err != nil {
		return nil, common.NewCustomError(nil, "High price not valid", "")
	}
	low, err := strconv.ParseFloat(row[idxes.low], 64)
	if err != nil {
		return nil, common.NewCustomError(nil, "Low price not valid", "")
	}
	close, err := strconv.ParseFloat(row[idxes.close], 64)
	if err != nil {
		return nil, common.NewCustomError(nil, "Close price not valid", "")
	}

	unixTimestamp, err := strconv.ParseInt(row[idxes.unix], 10, 64)
	if err != nil {
		return nil, common.NewCustomError(nil, "Unix timestamp not valid", "")
	}
	return &pricingmodel.Price{
		Symbol:        row[idxes.symbol],
		Open:          open,
		High:          high,
		Low:           low,
		Close:         close,
		UnixTimestamp: unixTimestamp,
	}, nil
}
