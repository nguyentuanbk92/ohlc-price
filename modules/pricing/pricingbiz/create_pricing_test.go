package pricingbiz

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/enums/symbol_type"
	"testing"

	"github.com/stretchr/testify/assert"
)

type pricingStoreMock struct{}

func (s *pricingStoreMock) Create(ctx context.Context, args *pricingmodel.Price) (*pricingmodel.Price, error) {
	return &pricingmodel.Price{}, nil
}

func TestCreatePrice(t *testing.T) {
	store := &pricingStoreMock{}
	pricingBiz := NewCreatePricingBiz(store)
	ctx := context.Background()

	testcases := []struct {
		name       string
		input      *pricingmodel.CreatePricingArgs
		errMessage string
	}{
		{
			name: "Missing high price",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				Low:           1,
				Open:          1,
				Close:         1,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrHighPriceNotValid.Message,
		},
		{
			name: "High price not valid",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				High:          -1,
				Low:           1,
				Open:          1,
				Close:         1,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrHighPriceNotValid.Message,
		},
		{
			name: "Missing low price",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				High:          1,
				Open:          1,
				Close:         1,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrLowPriceNotValid.Message,
		},
		{
			name: "Missing open price",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				High:          1,
				Low:           1,
				Close:         1,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrOpenPriceNotValid.Message,
		},
		{
			name: "Missing close price",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				High:          1,
				Low:           1,
				Open:          1,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrClosePriceNotValid.Message,
		},
		{
			name: "High price lower than low price",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				Open:          11000,
				High:          10000,
				Low:           15000,
				Close:         9000,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrHighPriceLowerThanLowPrice.Message,
		},
		{
			name: "Missing unix timestamp",
			input: &pricingmodel.CreatePricingArgs{
				Open:   1,
				High:   1,
				Low:    1,
				Close:  1,
				Symbol: symbol_type.BTCBUSD,
			},
			errMessage: pricingmodel.ErrUnixTimeStampNotValid.Message,
		},
		{
			name: "Missing symbol",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				Open:          1,
				High:          1,
				Low:           1,
				Close:         1,
			},
			errMessage: pricingmodel.ErrMissingSymbol.Message,
		},
		{
			name: "Symbol does not support",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				Open:          1,
				High:          1,
				Low:           1,
				Close:         1,
				Symbol:        "XRPUSDT",
			},
			errMessage: "Symbol XRPUSDT does not support",
		},
		{
			name: "Create success",
			input: &pricingmodel.CreatePricingArgs{
				UnixTimestamp: 1644719701000,
				Open:          1,
				High:          1,
				Low:           1,
				Close:         1,
				Symbol:        symbol_type.BTCBUSD,
			},
			errMessage: "",
		},
	}

	for _, tt := range testcases {
		t.Run(tt.name, func(t *testing.T) {
			_, err := pricingBiz.Create(ctx, tt.input)
			if err != nil {
				assert.Equal(t, tt.errMessage, err.Error())
			}
		})
	}
}
