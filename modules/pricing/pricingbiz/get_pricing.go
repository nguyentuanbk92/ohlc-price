package pricingbiz

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
	"ohlc-price/pkg/common"
)

type GetPricingStore interface {
	GetByCondition(ctx context.Context, conditions map[string]interface{}) (*pricingmodel.Price, error)
}

type getPricingBiz struct {
	store GetPricingStore
}

func NewGetPricingBiz(store GetPricingStore) *getPricingBiz {
	return &getPricingBiz{store: store}
}

func (biz *getPricingBiz) Get(ctx context.Context, id int) (*pricingmodel.Price, error) {
	result, err := biz.store.GetByCondition(ctx, map[string]interface{}{"id": id})
	if err != nil {
		return nil, common.ErrCannotListEntity(pricingmodel.EntityName, err)
	}

	return result, nil
}
