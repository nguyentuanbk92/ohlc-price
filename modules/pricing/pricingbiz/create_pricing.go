package pricingbiz

import (
	"context"
	"ohlc-price/modules/pricing/pricingmodel"
)

type CreatePricingStore interface {
	Create(ctx context.Context, args *pricingmodel.Price) (*pricingmodel.Price, error)
}

type createPricingBiz struct {
	store CreatePricingStore
}

func NewCreatePricingBiz(store CreatePricingStore) *createPricingBiz {
	return &createPricingBiz{store: store}
}

func (biz *createPricingBiz) Create(ctx context.Context, args *pricingmodel.CreatePricingArgs) (*pricingmodel.Price, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	price := &pricingmodel.Price{
		Symbol:        args.Symbol.String(),
		Open:          args.Open,
		High:          args.High,
		Low:           args.Low,
		Close:         args.Close,
		UnixTimestamp: args.UnixTimestamp,
	}
	return biz.store.Create(ctx, price)
}
