package subscriber

import (
	"context"
	importprocessbiz "ohlc-price/modules/importprocess/biz"
	importprocessmodel "ohlc-price/modules/importprocess/model"
	importprocessstorage "ohlc-price/modules/importprocess/storage"
	"ohlc-price/pkg/component"
	"ohlc-price/pkg/pubsub"
)

func HandleImportProcessing(appCtx component.AppContext) consumerJob {
	handler := func(ctx context.Context, message *pubsub.Message) error {
		data := message.Data().(*importprocessmodel.ImportProcessingEventData)

		imProcessStore := importprocessstorage.NewSQLStore(appCtx.GetMainDBConnection())
		createImProcessBiz := importprocessbiz.NewCreateImProcessBiz(imProcessStore)

		cmd := &importprocessmodel.CreateImportProcessArgs{
			State:          data.State,
			OriginFileName: data.OriginFileName,
			ImportType:     data.ImportType,
			UUID:           data.UUID,
		}

		return createImProcessBiz.Create(ctx, cmd)
	}
	return consumerJob{
		Title:   "Import process starting",
		Handler: handler,
	}
}

func HandleImportCompleted(appCtx component.AppContext) consumerJob {
	handler := func(ctx context.Context, message *pubsub.Message) error {
		data := message.Data().(*importprocessmodel.ImportProcessCompletedEventData)

		imProcessStore := importprocessstorage.NewSQLStore(appCtx.GetMainDBConnection())
		updateImProcessBiz := importprocessbiz.NewUpdateImProcessBiz(imProcessStore)

		cmd := &importprocessmodel.UpdateImportProcessInfoArgs{
			CompletedAt:  data.CompletedAt,
			State:        data.State,
			SuccessCount: data.SuccessCount,
			ErrorCount:   data.ErrorCount,
		}

		return updateImProcessBiz.UpdateInfoByUUID(ctx, data.UUID, cmd)
	}
	return consumerJob{
		Title:   "Import process completed",
		Handler: handler,
	}
}
