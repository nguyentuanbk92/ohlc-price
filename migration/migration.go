package migration

import (
	importprocessmodel "ohlc-price/modules/importprocess/model"
	"ohlc-price/modules/pricing/pricingmodel"

	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&pricingmodel.Price{})
	db.AutoMigrate(&importprocessmodel.ImportProcess{})
}
