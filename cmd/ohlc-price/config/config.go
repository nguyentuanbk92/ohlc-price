package config

import (
	"ohlc-price/pkg/common/cmenv"
	cc "ohlc-price/pkg/common/config"
)

type Config struct {
	HTTP     cc.HTTP     `yaml:"http"`
	Postgres cc.Postgres `yaml:"postgres"`
	Env      string      `yaml:"env"`
}

func Default() Config {
	cfg := Config{
		HTTP: cc.HTTP{Port: 8080},
		Postgres: cc.Postgres{
			Host:      "db",
			Port:      5432,
			Username:  "ohlc-price",
			Password:  "VpQj3=4n",
			Database:  "ohlc-price",
			DebugMode: true,
		},
		Env: cmenv.EnvDev.String(),
	}
	return cfg
}

// Load loads config from file
func Load() (Config, error) {
	var cfg, defCfg Config
	defCfg = Default()
	err := cc.LoadWithDefault(&cfg, defCfg)
	if err != nil {
		return cfg, err
	}
	cc.PostgresMustLoadEnv(&cfg.Postgres)
	return cfg, err
}
