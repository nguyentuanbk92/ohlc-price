package main

import (
	"log"
	"ohlc-price/cmd/ohlc-price/config"
	"ohlc-price/migration"
	ginimportprocess "ohlc-price/modules/importprocess/transport/gin"
	"ohlc-price/modules/pricing/pricingtransport/ginpricing"
	"ohlc-price/pkg/common/cmenv"
	cc "ohlc-price/pkg/common/config"
	"ohlc-price/pkg/component"
	"ohlc-price/pkg/middleware"
	"ohlc-price/pkg/pubsub/pslocal"
	"ohlc-price/subscriber"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/k0kubun/pp/v3"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	cc.InitFlags()
	cc.ParseFlags()

	// load config
	cfg, err := config.Load()

	if err != nil {
		log.Fatal("can not load config")
	}
	cmenv.SetEnvironment("ohlc-price-server", cfg.Env)
	if cmenv.IsDev() {
		pp.Println("config ::", cfg)
	}

	// connect db
	_, dsn := cfg.Postgres.ConnectionString()
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln(err)
	}

	if cfg.Postgres.DebugMode {
		db = db.Debug()
	}
	// migration
	migration.Migrate(db)

	if err := runService(cfg, db); err != nil {
		log.Fatalln(err)
	}
}

func runService(cfg config.Config, db *gorm.DB) error {
	localPubSub := pslocal.NewLocalPubSub()
	appCtx := component.NewAppContext(db, localPubSub)

	if err := subscriber.NewConsumerEngine(appCtx).Start(); err != nil {
		log.Fatalln(err)
	}

	r := gin.Default()
	r.Use(middleware.Recover(appCtx))
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	pricing := r.Group("/data")
	{
		pricing.POST("", ginpricing.HandleImportOHCLPrice(appCtx))
		pricing.GET("", ginpricing.ListPricing(appCtx))
	}
	r.POST("/price", ginpricing.CreatePricing(appCtx))

	r.GET("/import-process", ginimportprocess.ListImportProcess(appCtx))
	return r.Run(":" + strconv.Itoa(cfg.HTTP.Port))
}
