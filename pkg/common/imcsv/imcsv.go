package imcsv

import (
	"fmt"
	"ohlc-price/pkg/common"
)

type ColumnDef struct {
	Code     string
	Name     string
	NameNorm string
	Optional bool
	Index    *int
}

type Schema []*ColumnDef

func (schema Schema) MustValidate() error {
	for _, col := range schema {
		if col.Code == "" || col.Name == "" {
			return common.NewCustomError(nil, "invalid schema definition", "")
		}
	}
	return nil
}

func (schema Schema) ValidateHeader(headerRow []string) (map[string]*ColumnDef, error) {
	res := make(map[string]*ColumnDef)
	for _, col := range schema {
		for i, header := range headerRow {
			idx := i
			if header == col.Name {
				col.Index = &idx
				res[col.Code] = col
				continue
			}
			// TODO: map header by name_norm
		}
		if col.Index == nil {
			return nil, common.NewCustomError(
				nil,
				fmt.Sprintf("Header %v not found", col.Name),
				"")
		}
	}
	return res, nil
}
