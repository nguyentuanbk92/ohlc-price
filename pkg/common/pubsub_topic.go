package common

import "ohlc-price/pkg/pubsub"

const (
	TopicImportProcessing       pubsub.Topic = "TopicImportProcessing"
	TopicImportProcessCompleted pubsub.Topic = "TopicImportProcessCompleted"
)
