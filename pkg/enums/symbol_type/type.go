package symbol_type

type SymbolType string

const (
	Undefined SymbolType = ""
	BTCUSDT   SymbolType = "BTCUSDT"
	BTCBUSD   SymbolType = "BTCBUSD"
	ETHUSDT   SymbolType = "ETHUSDT"
	ETHBUSD   SymbolType = "ETHBUSD"
)

func (s SymbolType) String() string {
	return string(s)
}

func (s SymbolType) IsValid() bool {
	switch s {
	case BTCUSDT, BTCBUSD, ETHUSDT, ETHBUSD:
		return true
	default:
		return false
	}
}
