package pubsub

import (
	"fmt"
	"time"
)

type Message struct {
	id        string
	channel   Topic
	data      interface{}
	createdAt time.Time
}

func NewMessage(data interface{}) *Message {
	now := time.Now().UTC()

	return &Message{
		id:        fmt.Sprintf("%d", now.UnixNano()),
		data:      data,
		createdAt: now,
	}
}

func (evt *Message) Data() interface{} {
	return evt.data
}

func (evt *Message) SetChannel(topic Topic) {
	evt.channel = topic
}

func (evt *Message) Channel() Topic {
	return evt.channel
}
