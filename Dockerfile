FROM golang:1.18.1-alpine as builder
WORKDIR /app

RUN apk add git
RUN mkdir -p /build && mkdir -p /build/bin
COPY . .

RUN CGO_ENABLED=1 GOOS=linux go build \
      -o /build/bin/ohlc-price \
      -tags release \
      ./cmd/ohcl-price

FROM alpine

COPY --from=builder /build/bin/* /usr/bin/

EXPOSE 8080
